function U(e, n, T) {
  if (!e.getShaderParameter(n, e.COMPILE_STATUS))
    throw console.error(`Error in ${T} ` + e.getShaderInfoLog(n), !0), new Error("Shader error - see message");
}
function ie(e, n, T, a) {
  let m = e.createShader(e.VERTEX_SHADER);
  e.shaderSource(m, n), e.compileShader(m), U(e, m, "vertex shader");
  let _ = e.createShader(e.FRAGMENT_SHADER);
  if (e.shaderSource(_, T), e.compileShader(_), U(e, _, "fragment shader"), e.attachShader(a, m), e.attachShader(a, _), e.linkProgram(a), e.deleteShader(m), e.deleteShader(_), a.vertex_source = n, a.fragment_source = T, e.getProgramParameter(a, e.LINK_STATUS))
    a.linked = !0;
  else
    throw console.error("Could not initialize WebGL program"), "Couldn't link shader program - " + e.getProgramInfoLog(this);
}
function h(e, n) {
  let T = new Uint8Array(e.buffer, n, e.buffer.byteLength - n), a = T.indexOf(0);
  return new TextDecoder().decode(T.subarray(0, a));
}
function F(e, n, T) {
  return new Float32Array(e.buffer, n, T);
}
function se(e, n) {
  e.getExtension("EXT_color_buffer_float"), e.getExtension("EXT_float_blend");
  const T = (r, t, i, o) => {
    n.settings.clearColor[0] = r, n.settings.clearColor[1] = t, n.settings.clearColor[2] = i, n.settings.clearColor[3] = o, e.clearColor(r, t, i, o);
  }, a = (r) => e.enable(r), m = (r) => e.depthFunc(r), _ = (r) => e.clear(r), A = (r, t) => e.compileShader(r, t), x = (r) => e.linkProgram(r), w = (r, t) => e.glGetShaderParameter(r, t), P = (r, t) => e.BindBuffer(r, t), B = (r, t) => e.glGetAttribLocation(r, t), D = () => (n.buffers.push(e.createBuffer()), n.buffers.length - 1), S = (r, t, i) => {
    let o = n.buffers[r], s = F(window.memory, t, i);
    e.bindBuffer(e.ARRAY_BUFFER, o), e.bufferData(e.ARRAY_BUFFER, s, e.STATIC_DRAW), e.bindBuffer(e.ARRAY_BUFFER, null);
  }, C = (r, t, i, o, s, E = 3, u = e.FLOAT, c = e.FALSE, f = 0, d = 0) => {
    let re = n.buffers[t], te = n.vaos[r], ne = n.shaders[i], oe = h(window.memory, s);
    e.bindVertexArray(te), e.bindBuffer(e.ARRAY_BUFFER, re), e.enableVertexAttribArray(o), e.bindAttribLocation(ne, o, oe), e.vertexAttribPointer(o, E, u, c, f, d), e.bindBuffer(e.ARRAY_BUFFER, null), e.bindVertexArray(null);
  }, L = (r) => e.useProgram(r), X = (r) => e.enableVertexAttribArray(r), M = (r, t, i, o, s, E) => {
    e.vertexAttribPointer(r, t, i, o, s, E);
  }, I = (r, t, i) => {
    e.drawArrays(r, t, i);
  }, R = () => (n.textures.push(e.createTexture()), n.textures.length - 1), O = (r, t) => {
    e.activeTexture(e[`TEXTURE${t}`]), e.bindTexture(e.TEXTURE_2D, n.textures[r]);
  }, y = (r) => {
    let t = new Image();
    t.src = r, t.onload = () => {
      let i = R(), o = n.textures[i];
      return e.bindTexture(e.TEXTURE_2D, o), e.texImage2D(
        e.TEXTURE_2D,
        0,
        e.RGBA,
        t.width,
        t.height,
        0,
        e.RGBA,
        e.UNSIGNED_BYTE,
        t
      ), e.texParameteri(e.TEXTURE_2D, e.TEXTURE_MAG_FILTER, e.NEAREST), e.texParameteri(e.TEXTURE_2D, e.TEXTURE_MIN_FILTER, e.NEAREST), e.texParameteri(e.TEXTURE_2D, e.TEXTURE_WRAP_S, e.CLAMP_TO_EDGE), e.texParameteri(e.TEXTURE_2D, e.TEXTURE_WRAP_T, e.CLAMP_TO_EDGE), e.bindTexture(e.TEXTURE_2D, null), o.width = t.width, o.height = t.height, n.textures.push(o), i;
    };
  }, N = (r) => n.textures[r].width, G = (r) => n.textures[r].height, p = (r, t = e.RGBA32F, i = 200, o = 200, s = e.RGBA32F, E = e.FLOAT, u = null, c = 100) => {
    e.bindTexture(e.TEXTURE_2D, n.textures[r]), u !== null && (u = F(window.memory, u, c)), e.texParameteri(e.TEXTURE_2D, e.TEXTURE_MAG_FILTER, e.NEAREST), e.texParameteri(e.TEXTURE_2D, e.TEXTURE_MIN_FILTER, e.NEAREST), e.texParameteri(e.TEXTURE_2D, e.TEXTURE_WRAP_S, e.CLAMP_TO_EDGE), e.texParameteri(e.TEXTURE_2D, e.TEXTURE_WRAP_T, e.CLAMP_TO_EDGE), e.texImage2D(e.TEXTURE_2D, 0, t, i, o, 0, s, E, u);
  }, v = (r = e.RGBA32F, t = e.RGBA32F, i = e.FLOAT, o = null) => {
    e.bindTexture(e.TEXTURE_2D, n.textures[0]), o !== null && (o = F(window.memory, o));
    let s = e.canvas.width, E = e.canvas.height;
    e.texParameteri(e.TEXTURE_2D, e.TEXTURE_MAG_FILTER, e.NEAREST), e.texParameteri(e.TEXTURE_2D, e.TEXTURE_MIN_FILTER, e.NEAREST), e.texParameteri(e.TEXTURE_2D, e.TEXTURE_WRAP_S, e.CLAMP_TO_EDGE), e.texParameteri(e.TEXTURE_2D, e.TEXTURE_WRAP_T, e.CLAMP_TO_EDGE), e.texImage2D(e.TEXTURE_2D, 0, r, s, E, 0, t, i, o);
  }, V = () => (n.vaos.push(
    e.createVertexArray()
  ), n.vaos.length - 1), W = (r) => {
    e.bindVertexArray(n.vaos[r]);
  }, l = (r, t, i) => {
    let o = h(window.memory, r), s = h(window.memory, t), E = n.shaders[i];
    ie(e, o, s, E);
    let u = e.getProgramParameter(E, e.ACTIVE_UNIFORMS);
    for (let c = 0; c < u; ++c) {
      let f = e.getActiveUniform(E, c), d = e.getUniformLocation(E, f.name);
      E.uniforms[f.name] = {
        location: d,
        name: f.name
      };
    }
    n.shaders[i] = E;
  }, H = () => {
    let r = e.createProgram();
    return r.uniforms = {}, n.shaders.push(r), n.shaders.length - 1;
  }, k = (r) => {
    let t = n.shaders[r];
    e.useProgram(t);
  }, Y = (r) => {
    let t = n.shaders[r];
    return e.getProgramParameter(t, e.ACTIVE_UNIFORMS);
  }, $ = (r, t) => e.glGetUniformLocation(r, t), j = (r, t) => e.glUniform4fv(r, t), z = (r, t, i) => {
    let o = n.shaders[r], s = h(window.memory, t);
    b(o, r, s) && e.uniform1f(o.uniforms[s].location, i);
  }, K = (r, t, i) => {
    let o = n.shaders[r], s = h(window.memory, t);
    b(o, r, s) && e.uniform1i(o.uniforms[s].location, i);
  }, q = (r, t, i, o) => {
    let s = n.shaders[r], E = h(window.memory, t);
    b(s, r, E) && e.uniform2fv(s.uniforms[E].location, [i, o]);
  };
  function b(r, t, i) {
    return r.uniforms.hasOwnProperty(i) ? !0 : (console.error(
      `Unable to find the uniform 

%c"${i}" 

 %cin shader with id of ${t}`,
      "color:yellow"
    ), !1);
  }
  const J = (r) => {
    let t = n.framebuffers[r];
    e.bindFramebuffer(e.FRAMEBUFFER, t);
  }, Q = () => {
    e.bindFramebuffer(e.FRAMEBUFFER, null);
  }, Z = () => (n.framebuffers.push(
    e.createFramebuffer()
  ), n.framebuffers.length - 1), g = (r, t, i = 0) => {
    let o = n.framebuffers[r], s = n.textures[t];
    e.bindTexture(e.TEXTURE_2D, s), e.bindFramebuffer(e.FRAMEBUFFER, o), e.framebufferTexture2D(e.FRAMEBUFFER, e.COLOR_ATTACHMENT0 + i, e.TEXTURE_2D, s, 0);
    let E = e.checkFramebufferStatus(e.FRAMEBUFFER);
    ee(E), e.bindFramebuffer(e.FRAMEBUFFER, null);
  }, ee = (r) => {
    switch (r) {
      case e.FRAMEBUFFER_UNSUPPORTED:
        throw new Error("FBO : Framebuffer unsupported");
      case e.FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
        throw new Error("FBO : Framebuffer incomplete attachment");
      case e.FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
        throw new Error("FBO : Framebuffer incomplete dimensions");
      case e.FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
        throw new Error("FBO : Framebuffer incomplete missing attachment");
      case e.FRAMEBUFFER_COMPLETE:
        return !0;
      default:
        return console.error("unknown error creating framebuffer"), !1;
    }
  };
  return {
    glClearColor: T,
    glEnable: a,
    glDepthFunc: m,
    glClear: _,
    glCompileShader: A,
    glLinkProgram: x,
    glGetShaderParameter: w,
    glBindBuffer: P,
    glGetAttribLocation: B,
    glGetUniformLocation: $,
    glUniform4fv: j,
    glCreateFramebuffer: Z,
    glCreateBuffer: D,
    glCompileBuffer: S,
    glUseProgram: L,
    glEnableVertexAttribArray: X,
    glVertexAttribPointer: M,
    glDrawArrays: I,
    glCreateTexture: R,
    glBindTexture2d: O,
    glCompileShaderProgram: l,
    glCreateProgram: H,
    glCreateVao: V,
    glLinkAttribLocation: C,
    glBindShader: k,
    glBindVertexArray: W,
    glTexImage2D: p,
    glFramebufferAttachment: g,
    glBindFramebuffer: J,
    unbindFramebuffer: Q,
    glTexImage2DFullscreen: v,
    glViewport: (r, t, i, o) => {
      e.viewport(r, t, i, o);
    },
    glViewportFullscreen: (r, t) => {
      e.viewport(r, t, window.innerWidth, window.innerHeight);
    },
    getNumUniforms: Y,
    glUniform1i: K,
    glUniform1f: z,
    glUniform2fv: q,
    clearScreen: () => {
      e.viewport(0, 0, window.innerWidth, window.innerHeight), e.clearColor(
        n.settings.clearColor[0],
        n.settings.clearColor[1],
        n.settings.clearColor[2],
        n.settings.clearColor[3]
      ), e.clear(e.COLOR_BUFFER_BIT | e.DEPTH_BUFFER_BIT);
    },
    clearCommonBits: () => {
      e.clear(e.COLOR_BUFFER_BIT | e.DEPTH_BUFFER_BIT);
    },
    enableAlphaBlending: () => {
      e.enable(e.BLEND), e.blendFunc(e.SRC_ALPHA, e.ONE_MINUS_SRC_ALPHA);
    },
    disableBlending: () => {
      e.disable(e.BLEND);
    },
    glDebugTex: (r, t, i) => {
      let o = 262144, s = n.textures[r];
      e.bindTexture(e.TEXTURE_2D, s);
      let E = F(window.memory, t, i), u = new Float32Array(o * 4), c = 0;
      for (let f = 0; f < o; f++)
        c = f * 4, u[c++] = Math.random(), u[c++] = Math.random(), u[c++] = Math.random(), u[c++] = 1;
      e.texParameteri(e.TEXTURE_2D, e.TEXTURE_MAG_FILTER, e.NEAREST), e.texParameteri(e.TEXTURE_2D, e.TEXTURE_MIN_FILTER, e.NEAREST), e.texParameteri(e.TEXTURE_2D, e.TEXTURE_WRAP_S, e.CLAMP_TO_EDGE), e.texParameteri(e.TEXTURE_2D, e.TEXTURE_WRAP_T, e.CLAMP_TO_EDGE), e.texImage2D(e.TEXTURE_2D, 0, e.RGBA32F, 512, 512, 0, e.RGBA, e.FLOAT, E);
    },
    loadImageTexture: y,
    enableBlend: () => {
      e.enable(e.BLEND);
    },
    blendFunc: (r, t) => {
      e.blendFunc(r, t);
    },
    getTextureWidth: N,
    getTextureHeight: G
  };
}
const Ae = {
  /**
   * Initializes a WebAssembly module and exports the WebGL functions to that module.
   * @param gl {WebGL2RenderingContext} a WebGL context
   * @param wasm_path {string} the path to the WASM module to load
   * @param cb {Function} a callback function to run once the WASM module is loaded.
   */
  init_webassembly(e, n, T) {
    const a = {}, m = se(e, this.data);
    Object.assign(a, m), Object.assign(a, this.dom), window.gl = e, WebAssembly.instantiateStreaming(fetch(n), { env: a }).then((_) => {
      let A = _.instance;
      window.memory = A.exports.memory, T(A);
    });
  },
  /**
   * Export of DOM api methods to expose to WASM
   */
  dom: {
    getWindowWidth() {
      return window.innerWidth;
    },
    getWindowHeight() {
      return window.innerHeight;
    },
    logValue(...e) {
      console.log(e.join("|"));
    }
  },
  /**
   * In order for this to work, there needs to be a global store of elements,
   * When you make a new buffer,texture,etc., it gets added to the store and you can reference it with an index.
   */
  data: {
    buffers: [],
    textures: [],
    framebuffers: [],
    vaos: [],
    shaders: [],
    settings: {
      clearColor: [0, 0, 0, 1]
    }
  }
};
export {
  Ae as default
};
