import {resolve} from "path"
import {defineConfig} from "vite";

export default defineConfig({
    build: {
        outDir:"site/dist",
        lib: {
            entry: resolve(__dirname, 'src/main.js'),
            name: 'zgl',
            fileName: (format) => `zgl.${format}.js`
        }
    }
})