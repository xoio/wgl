WGL
===

This is a wrapper library around parts of the WebGL api for use with WASM implementations. 


Building
==
* `npm install`
* `npm run build`
* Will output library into `site/dist`. 


Example Usage
=====
```javascript

import zgl from "./dist/zgl.es"

const canvas = document.createElement("canvas")
const gl = canvas.getContext("webgl2")

zgl.init_webassembly(gl, "./zig_gl.wasm", (instance) => {

    let funcs = instance.exports
    let start = Date.now()
    instance.exports.setup();

    funcs.setup();

    const animate = function(ts){
        let time = (Date.now() - start) * 0.001;
        requestAnimationFrame(animate);
        funcs.draw(time)
    }

    animate();

    window.addEventListener("resize",()=> {
        funcs.resize(window.innerWidth,window.innerHeight);
        gl.canvas.width = window.innerWidth;
        gl.canvas.height = window.innerHeight
    });

})

```
