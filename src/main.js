import glexports from "./glexports";

export default {

    /**
     * Initializes a WebAssembly module and exports the WebGL functions to that module.
     * @param gl {WebGL2RenderingContext} a WebGL context
     * @param wasm_path {string} the path to the WASM module to load
     * @param cb {Function} a callback function to run once the WASM module is loaded.
     */
    init_webassembly(gl, wasm_path, cb) {
        const env = {}

        const exports = glexports(gl, this.data);
        Object.assign(env, exports)
        Object.assign(env, this.dom);

        // make context global since we might need it for other things.
        window.gl = gl

        WebAssembly.instantiateStreaming(fetch(wasm_path), {env})
            .then(results => {
                let instance = results.instance;

                // also make memory global for ease-of-access
                window.memory = instance.exports.memory;
                cb(instance)
            })
    },

    /**
     * Export of DOM api methods to expose to WASM
     */
    dom: {
        getWindowWidth() {
            return window.innerWidth
        },
        getWindowHeight() {
            return window.innerHeight
        },
        logValue(...val) {
            console.log(val.join("|"))
        }
    },

    /**
     * In order for this to work, there needs to be a global store of elements,
     * When you make a new buffer,texture,etc., it gets added to the store and you can reference it with an index.
     */
    data: {
        buffers: [],
        textures: [],
        framebuffers: [],
        vaos: [],
        shaders: [],
        settings: {
            clearColor: [0, 0, 0, 1]
        }
    },
}