import {compileShader} from "./glshader";
import {memoryAddressToString, memoryAddressToFloat32Array} from "./utils"

/**
 * Exports functions to pass along for use within
 * Zig
 * @param {WebGLRenderingContext} gl
 * @param {Object} dataRef the object to use to store client side objects like buffers and textures.
 * @returns
 */
export default function (gl, dataRef) {

    // enable some extensions we might need.
    gl.getExtension("EXT_color_buffer_float");
    gl.getExtension("EXT_float_blend");

    /**
     * Sets the clear color
     * @param {number} r the red value
     * @param {number} g the green value
     * @param {number} b the blue value
     * @param {number} a the alpha value
     */
    const glClearColor = (r, g, b, a) => {
        dataRef.settings.clearColor[0] = r;
        dataRef.settings.clearColor[1] = g;
        dataRef.settings.clearColor[2] = b;
        dataRef.settings.clearColor[3] = a;

        gl.clearColor(r, g, b, a);

    }
    const glEnable = x => gl.enable(x);
    const glDepthFunc = x => gl.depthFunc(x);
    const glClear = x => gl.clear(x);
    const glCompileShader = (type, source) => gl.compileShader(type, source);
    const glLinkProgram = (prog) => gl.linkProgram(prog);
    const glGetShaderParameter = (shader, status) => gl.glGetShaderParameter(shader, status);
    const glBindBuffer = (type, buffer) => gl.BindBuffer(type, buffer);
    const glGetAttribLocation = (prog, name) => gl.glGetAttribLocation(prog, name);

    ///////// VBO RELATED ////////

    // stores buffer in global store; returns index as id.
    const glCreateBuffer = () => {
        dataRef.buffers.push(gl.createBuffer())
        return dataRef.buffers.length - 1;
    }

    // links data from zig with buffer object here on the client side.
    const glCompileBuffer = (id, data, length) => {
        let buffer = dataRef.buffers[id];
        let _data = memoryAddressToFloat32Array(window.memory, data, length);

        gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
        gl.bufferData(gl.ARRAY_BUFFER, _data, gl.STATIC_DRAW);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
    }

    /**
     * Links an attribute to an attribute location in a shader.
     * @param {number} vao_id the id of the vao to associate this link with
     * @param {number} buffer_id the id of the buffer who's data you want to associate with this location
     * @param {number} shader_id the id of the shader to utilize for the data in the specified buffer
     * @param {number} location the attribute location in the shader the specified buffer will be used in
     * @param {string} shader_name the name of the attribute in the shader
     * @param {number} component_size the size of each item in the buffer. ie vec4, vec3, vec2 corresponds to 4,3,2. Default is 3
     * @param {number} type The type of data used in the buffer. Defaults to float
     * @param {number} normalized Whether or not the data in the buffer is normalized. Defaults to false
     * @param {number} stride  The stride of each data in the buffer
     * @param {number} offset  The offset of each bit of data in the buffer.
     */
    const glLinkAttribLocation = (
        vao_id,
        buffer_id,
        shader_id,
        location,
        shader_name,
        component_size = 3,
        type = gl.FLOAT,
        normalized = gl.FALSE,
        stride = 0,
        offset = 0
    ) => {
        let buffer = dataRef.buffers[buffer_id];
        let vao = dataRef.vaos[vao_id]
        let shader = dataRef.shaders[shader_id];
        let name = memoryAddressToString(window.memory, shader_name);


        gl.bindVertexArray(vao);
        gl.bindBuffer(gl.ARRAY_BUFFER, buffer);

        gl.enableVertexAttribArray(location);
        gl.bindAttribLocation(shader, location, name);
        gl.vertexAttribPointer(location, component_size, type, normalized, stride, offset);

        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        gl.bindVertexArray(null);
    }

    function debugUv() {
        let size = 512;
        let count = 512 * 512;
        let uvs = new Float32Array(count * 2);


        let id = 0,
            u,
            v;
        for (let i = 0; i < count; i++) {
            //point cloud vertex

            //computes the uvs
            u = (i % size) / size;
            v = (i / size) / size;
            id = i * 2;
            uvs[id++] = u;
            uvs[id] = v;
        }

        return uvs;
    }

    ///////////// SHADER RELATED ////////////

    const glUseProgram = (program) => gl.useProgram(program);
    const glEnableVertexAttribArray = (index) => gl.enableVertexAttribArray(index);
    const glVertexAttribPointer = (index, size, type, normalized, stride, offset) => {
        gl.vertexAttribPointer(index, size, type, normalized, stride, offset)
    }
    const glDrawArrays = (mode, first, count) => {
        gl.drawArrays(mode, first, count);
    }

    ////////// TEXTURES /////////////////

    // stores texture in global store. Returns index as id.
    const glCreateTexture = () => {
        dataRef.textures.push(gl.createTexture());
        return dataRef.textures.length - 1;
    }

    // binds a 2d texture.
    const glBindTexture2d = (id, index) => {
        gl.activeTexture(gl[`TEXTURE${index}`]);
        gl.bindTexture(gl.TEXTURE_2D, dataRef.textures[id]);
    }

    /**
     * Loads an image as a texture
     * @param path {string} path of texture.
     */
    const loadImageTexture = (path) => {
        let img = new Image();

        path = memoryAddressToString(memory, path);
        let id = 0

        img.src = path;
        img.onload = () => {

            id = glCreateTexture()
            let tex = dataRef.textures[id];

            gl.bindTexture(gl.TEXTURE_2D, tex);

            // set the image
            // known to work with random floating point data
            //gl.texImage2D(this.format.type,0,gl.RGBA16F,width,height,0,gl.RGBA,gl.FLOAT,data);
            gl.texImage2D(
                gl.TEXTURE_2D,
                0,
                gl.RGBA,
                img.width,
                img.height,
                0,
                gl.RGBA,
                gl.UNSIGNED_BYTE,
                img
            );

            // set min and mag filters
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);

            //set wrapping
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)

            gl.bindTexture(gl.TEXTURE_2D, null);

            // bind width/height to texture if we need it
            tex.width = img.width
            tex.height = img.height
            tex.id = id;
        }

        return id

    }

    // Returns texture width. Primarily for when loading an image texture
    const getTextureWidth = (id) => {
        return dataRef.textures[id].width
    }

    // Returns texture height. Primarily for when loading an image texture
    const getTextureHeight = (id) => {
        return dataRef.textures[id].height
    }

    /**
     * Sets various properties of the texture.
     * See https://developer.mozilla.org/en-US/docs/Web/API/WebGLRenderingContext/texImage2D
     *
     * @param {number} texture_id the texture id to look up in the data store
     * @param {number} internalFormat internal format for the texture
     * @param {number} width width for the texture
     * @param {number} height height for the texture
     * @param {number} format texture format for the texture
     * @param {number} type the underlying datatype of the content of the texture
     * @param {ArrayBufferView} data the actual texture data
     */
    const glTexImage2D = (
        texture_id,
        internalFormat = gl.RGBA32F,
        width = 200,
        height = 200,
        format = gl.RGBA32F,
        type = gl.FLOAT,
        data = null,
        dataLen = 100
    ) => {


        gl.bindTexture(gl.TEXTURE_2D, dataRef.textures[texture_id])

        // if data isn't null, get actual values from WASM bundle.
        if (data !== null) {
            data = memoryAddressToFloat32Array(window.memory, data, dataLen);
        }

        // console.log(dataLen)
        // set default min/mag filter + wrapping.
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

        gl.texImage2D(gl.TEXTURE_2D, 0, internalFormat, width, height, 0, format, type, data);
    }


    const glTexImage2DFullscreen = (
        internalFormat = gl.RGBA32F,
        format = gl.RGBA32F,
        type = gl.FLOAT,
        data = null
    ) => {


        gl.bindTexture(gl.TEXTURE_2D, dataRef.textures[0])

        // if data isn't null, get actual values from WASM bundle.
        if (data !== null) {
            data = memoryAddressToFloat32Array(window.memory, data);
        }

        let width = gl.canvas.width;
        let height = gl.canvas.height;

        // set default min/mag filter + wrapping.
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

        gl.texImage2D(gl.TEXTURE_2D, 0, internalFormat, width, height, 0, format, type, data);

    }
    ////////// VAO RELATED /////////////
    const glCreateVao = () => {
        dataRef.vaos.push(
            gl.createVertexArray()
        );
        return dataRef.vaos.length - 1;
    }

    const glBindVertexArray = (id) => {
        gl.bindVertexArray(dataRef.vaos[id])
    }

    ///////// SHADER RELATED /////////
    const glCompileShaderProgram = (
        vertex_source,
        fragment_source,
        program_id
    ) => {

        //@ts-ignore
        let vertex_shader = memoryAddressToString(window.memory, vertex_source);
        //@ts-ignore
        let fragment_shader = memoryAddressToString(window.memory, fragment_source);
        let shader = dataRef.shaders[program_id];

        compileShader(gl, vertex_shader, fragment_shader, shader);

        // parse out active uniform and store onto object
        let numUniforms = gl.getProgramParameter(shader, gl.ACTIVE_UNIFORMS);

        // first loop through and save all active uniforms.
        for (let i = 0; i < numUniforms; ++i) {
            let info = gl.getActiveUniform(shader, i);
            let location = gl.getUniformLocation(shader, info.name);

            shader.uniforms[info.name] = {
                location: location,
                name: info.name
            };
        }

        dataRef.shaders[program_id] = shader;
    }

    const glCreateProgram = () => {
        let shader = gl.createProgram();
        shader["uniforms"] = {};
        dataRef.shaders.push(shader);
        return dataRef.shaders.length - 1;
    }

    const glBindShader = (id) => {
        let shader = dataRef.shaders[id];
        gl.useProgram(shader);
    }

    const getNumUniforms = (shader_id) => {
        let shader = dataRef.shaders[shader_id];
        return gl.getProgramParameter(shader, gl.ACTIVE_UNIFORMS);
    }

    const glGetUniformLocation = (prog, name) => gl.glGetUniformLocation(prog, name);
    const glUniform4fv = (loc, val) => gl.glUniform4fv(loc, val);

    const glUniform1f = (shader_id, name, value) => {
        let shader = dataRef.shaders[shader_id];
        let uniform_name = memoryAddressToString(window.memory, name);

        if (!checkUniform(shader, shader_id, uniform_name)) {
            return;
        }

        gl.uniform1f(shader.uniforms[uniform_name].location, value);
    }


    /**
     * Applies a float uniform value onto a shader.
     * @param {number} shader_id id of the shader to apply uniform to
     * @param {numer} name memory address of the uniform name
     * @param {number} value value for the uniform
     * @returns
     */
    const glUniform1i = (shader_id, name, value) => {
        let shader = dataRef.shaders[shader_id];
        let uniform_name = memoryAddressToString(window.memory, name);

        if (!checkUniform(shader, shader_id, uniform_name)) {
            return;
        }

        gl.uniform1i(shader.uniforms[uniform_name].location, value);
    }

    /**
     * Applies a vec2 uniform value onto a shader.
     * @param {number} shader_id id of the shader to apply uniform to
     * @param {numer} name memory address of the uniform name
     * @param {number} v1 x value for vec2
     * @param {number} v2 y value for vec2
     * @returns
     */
    const glUniform2fv = (shader_id, name, v1, v2) => {
        let shader = dataRef.shaders[shader_id];
        let uniform_name = memoryAddressToString(window.memory, name);

        if (!checkUniform(shader, shader_id, uniform_name)) {
            return;
        }

        gl.uniform2fv(shader.uniforms[uniform_name].location, [v1, v2]);
    }

    function checkUniform(shader, shader_id, uniform_name) {
        if (!shader.uniforms.hasOwnProperty(uniform_name)) {
            console.error(
                `Unable to find the uniform \n\n%c"${uniform_name}" \n\n %cin shader with id of ${shader_id}`, "color:yellow");

            return false
        }

        return true;
    }

    //////// FBO RELATED ///////////
    const glBindFramebuffer = (id) => {
        let fbo = dataRef.framebuffers[id];
        gl.bindFramebuffer(gl.FRAMEBUFFER, fbo);
        //gl.viewport(0,0,1024,768);
    }

    const unbindFramebuffer = () => {
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    }

    const glCreateFramebuffer = () => {

        dataRef.framebuffers.push(
            gl.createFramebuffer()
        );
        return dataRef.framebuffers.length - 1;

    }

    const glFramebufferAttachment = (fbo_id, texture_id, attachment_index = 0) => {
        let fbo = dataRef.framebuffers[fbo_id];
        let tex = dataRef.textures[texture_id];

        gl.bindTexture(gl.TEXTURE_2D, tex);
        gl.bindFramebuffer(gl.FRAMEBUFFER, fbo);
        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0 + attachment_index, gl.TEXTURE_2D, tex, 0);

        // check framebuffer status
        let status = gl.checkFramebufferStatus(gl.FRAMEBUFFER);

        checkFbo(status);

        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    }

    const checkFbo = (status) => {

        switch (status) {
            case gl.FRAMEBUFFER_UNSUPPORTED:
                throw new Error(`FBO : Framebuffer unsupported`)
            case gl.FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
                throw new Error(`FBO : Framebuffer incomplete attachment`)
            case gl.FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
                throw new Error(`FBO : Framebuffer incomplete dimensions`)
            case gl.FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
                throw new Error(`FBO : Framebuffer incomplete missing attachment`)

            case gl.FRAMEBUFFER_COMPLETE:
                return true;
            default:
                console.error("unknown error creating framebuffer")
                return false;
        }
    }


    const glViewport = (x, y, width, height) => {
        gl.viewport(x, y, width, height);
    }

    const glViewportFullscreen = (x, y) => {
        gl.viewport(x, y, window.innerWidth, window.innerHeight);
    }


    /**
     * Clears the screen based on current settings. Assumes fullscreen clear.
     */
    const clearScreen = () => {
        gl.viewport(0, 0, window.innerWidth, window.innerHeight)
        gl.clearColor(
            dataRef.settings.clearColor[0],
            dataRef.settings.clearColor[1],
            dataRef.settings.clearColor[2],
            dataRef.settings.clearColor[3],
        );
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    }

    const clearCommonBits = () => {
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    }

    const enableBlend = () => {
        gl.enable(gl.BLEND)
    }

    const blendFunc = (f1, f2) => {
        gl.blendFunc(f1, f2);
    }

    const enableAlphaBlending = () => {
        gl.enable(gl.BLEND);
        gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
    }

    const disableBlending = () => {
        gl.disable(gl.BLEND);
    }

    const glDebugTex = (tex_id, content, len) => {

        let count = 512 * 512;
        let tex = dataRef.textures[tex_id];
        gl.bindTexture(gl.TEXTURE_2D, tex)
        let array = memoryAddressToFloat32Array(window.memory, content, len);

        // if data isn't null, get actual values from WASM bundle.
        let data = new Float32Array(count * 4);
        let size = 512; // particles amount = ( size ^ 2 )


        let id = 0;
        for (let i = 0; i < count; i++) {

            id = i * 4;
            data[id++] = Math.random(); // normalized pos x
            data[id++] = Math.random(); // normalized pos y
            data[id++] = Math.random(); // normalized angle
            data[id++] = 1.0;
        }

        // set default min/mag filter + wrapping.
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA32F, 512, 512, 0, gl.RGBA, gl.FLOAT, array);
        //gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA32F,512,512,0,gl.RGBA,gl.FLOAT,data);

    }

    return {
        glClearColor,
        glEnable,
        glDepthFunc,
        glClear,
        glCompileShader,
        glLinkProgram,
        glGetShaderParameter,
        glBindBuffer,
        glGetAttribLocation,
        glGetUniformLocation,
        glUniform4fv,
        glCreateFramebuffer,
        glCreateBuffer,
        glCompileBuffer,
        glUseProgram,
        glEnableVertexAttribArray,
        glVertexAttribPointer,
        glDrawArrays,
        glCreateTexture,
        glBindTexture2d,
        glCompileShaderProgram,
        glCreateProgram,
        glCreateVao,
        glLinkAttribLocation,
        glBindShader,
        glBindVertexArray,
        glTexImage2D,
        glFramebufferAttachment,
        glBindFramebuffer,
        unbindFramebuffer,
        glTexImage2DFullscreen,
        glViewport,
        glViewportFullscreen,
        getNumUniforms,
        glUniform1i,
        glUniform1f,
        glUniform2fv,
        clearScreen,
        clearCommonBits,
        enableAlphaBlending,
        disableBlending,
        glDebugTex,
        loadImageTexture,
        enableBlend,
        blendFunc,
        getTextureWidth,
        getTextureHeight
    }
}